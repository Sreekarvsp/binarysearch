/**
 * Created by sreekarryallapragada on 2/25/20.
 *
 * How many times sorted array is rotated
 */
public class SortedArrayRotation {

    public static void main(String[] args)
    {
       // int[] nums = {11,12,15,18,2,5,6,8};
        int[] nums = {2,3,4,5,1};
        System.out.print("Array is rotated: " +findRotationCount(nums) + " times");
    }

    private static int findRotationCount(int[] nums) {

        int low=0; int high = nums.length-1;

        while(low <= high){

            //case 1: When array is sorted
            if(nums[low] <= nums[high]){
                return low;
            }

            //case 2: Pivot element -> next and prev are both greater
            int mid = (low+high)/2;
            /*int next = (mid+1) % nums.length;
            int prev = (mid + nums.length-1) % nums.length;
            if(nums[mid] <= nums[next] && nums[mid] <= nums[prev]){
                return mid;
            }*/

            // if the mid element is greater than its next element then mid+1 element is the smallest
            // This point would be the point of change. From higher to lower value.
            if (nums[mid] > nums[mid + 1]) {
                return mid + 1;
            }

            // If the mid element is lesser than its previous element then mid element is the smallest
            if (nums[mid - 1] > nums[mid]) {
                return mid;
            }

            //case 3
            else if(nums[mid] <= nums[high]){
                //Right half is sorted, so search in left half
                high = mid -1;
            }
            //case 4
            else if(nums[low] <= nums[mid]){
                //left half is sorted, search in right half
                low = mid + 1;
            }
        }

        return -1;
    }


}
