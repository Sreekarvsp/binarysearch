/**
 * Created by sreekarryallapragada on 2/25/20.
 *
 * Find element in circularly sorted array
 *
 * binary search - O(log n)
 *
 */
public class CircularSortedArray {

    public static void main(String[] args){

        int[] circularArray = {12,14,18,21,3,6,8,9};
        int target = 12;
        System.out.print(" Element " + target + " found at: " + circularArraySearch(circularArray,target));
    }

    public static int circularArraySearch(int[] cArray, int target){

        int low = 0; int high = cArray.length-1;

        while(low <= high){

            int mid = (low + high)/2;

            if(cArray[mid] == target){
                return mid;
            }
            //mid ele: 21
            //one of the halfs of the array is sorted, if target element is found in sorted array then return
            //else search for element in unsorted array

            //compare mid element and right most element, if mid element is less than right most element then
            // this half of array is sorted
            else if(cArray[mid] <=  cArray[high]){
                //right half is sorted
                if(target > cArray[mid] && target <= cArray[high]){
                    low = mid + 1; // search in right sorted half
                }else{
                    high = mid -1; // search in left half
                }
            }else{
                //left half is sorted

                if(target >= cArray[low] && target < cArray[mid]){
                    high = mid-1; //search in left sorted half
                }else{
                    low = mid + 1; // search in right half
                }
            }
        }

        return -1;
    }
}
