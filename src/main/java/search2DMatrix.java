/**
 * Created by sreekarryallapragada on 2/26/20.
 */
public class search2DMatrix {

    public static void main(String[] args) {

        int mat[][] = {{0, 6, 8, 9, 11},
                {20, 22, 28, 29, 31},
                {36, 38, 50, 61, 63},
                {64, 66, 100, 122, 128}};
        int target = 11;
        System.out.println(target + " is present in matrix? : " + searchMatrix(mat, target));
    }

    private static boolean searchMatrix(int[][] matrix, int target) {

        //Start our pointer in bottom left
        int row = matrix.length - 1;
        int column = 0;
        //matrix.length gives row length
        //matrix[0].length gives column length
        // System.out.println("Row length: " + row); System.out.println("Column length: " + column);
        // System.out.println("Element at " + row + " and " + column + ":"+ mat[row][column]);

        while (row >= 0 && column <= matrix[0].length) {

            if (matrix[row][column] == target) {
                return true;
            } else if (matrix[row][column] > target) {
                row--;
            } else if (matrix[row][column] < target) {
                column++;
            }
        }

        return false;
    }
}
