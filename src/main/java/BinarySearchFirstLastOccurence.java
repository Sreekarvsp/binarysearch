/**
 * Created by sreekarryallapragada on 2/24/20.
 */
public class BinarySearchFirstLastOccurence {

    public static void main(String[] args) {
        int[] arr = {2, 3, 10, 10, 10, 6, 7};
        System.out.println("First occurrence at: "+firstOccurrence( arr, 10));

        System.out.println("Last occurrence at: "+lastOccurrence( arr, 10));
    }

    public static int firstOccurrence(int[] arr, int target)
    {
        int lowIndex = 0;
        int highIndex = arr.length-1;
        int result = -1;
        while(lowIndex <= highIndex)
        {
            int mid = (lowIndex + highIndex) / 2;

            if(target == arr[mid])
            {
                result = mid;
                // search for lowest occurrence of the element on the left side of the array
                highIndex = mid - 1;
            }
            if(target < arr[mid])
            {
                highIndex = mid -1;
            }
            else if(target > arr[mid])
            {
               lowIndex = mid + 1;
            }
        }
        return result;
    }

    public static int lastOccurrence(int[] arr, int target)
    {
        int lowIndex = 0;
        int highIndex = arr.length-1;
        int result = -1;
        while(lowIndex <= highIndex)
        {
            int mid = (lowIndex + highIndex) / 2;

            if(target == arr[mid])
            {
                result = mid;
                // search for highest occurrence of the element on the right side of the array
                lowIndex = mid +1;
            }
            if(target < arr[mid])
            {
                highIndex = mid -1;
            }
            else if(target > arr[mid])
            {
                lowIndex = mid + 1;
            }
        }
        return result;
    }
}
