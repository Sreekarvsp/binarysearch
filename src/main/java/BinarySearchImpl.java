/**
 * Created by sreekarryallapragada on 2/24/20.
 */
public class BinarySearchImpl {

    public static void main(String[] args) {
        int[] sortedArray = new int[10];
        for (int i = 0; i < 10; i++) {
            sortedArray[i] = i;
        }

       // System.out.println("value present at index: " + BinarySearchImpl(sortedArray, 5));

        System.out.println("Recursion - Value present at index: " + binarySearchImplRecursive
                (0, sortedArray.length - 1, sortedArray, 8));

    }

    private static int binarySearchImplRecursive(int lowIndex, int highIndex, int[] array, int target) {

        int mid = (lowIndex + highIndex) / 2;

        if (array[mid] == target) {
            return mid;
        } else if (target < array[mid]) {
            return binarySearchImplRecursive(lowIndex, mid - 1, array, target);
        } else if (target > array[mid]) {
           return  binarySearchImplRecursive(mid + 1, highIndex, array, target);
        }
        return 0;
    }


    public static int BinarySearchImpl(int[] array, int target) {

        int arrlength = array.length;
        int high = arrlength - 1;
        int low = 0;


        //If there is only one element in array then low and high are same
        while (low <= high) {
            int mid = low + (high - low) / 2;

            if (array[mid] == target) {
                return mid;
            }
            if (target < array[mid]) {
                high = mid - 1;
            } else if (target > array[mid]) {
                low = mid + 1;
            }
        }

        return -1;
    }
}
