/**
 * Created by sreekarryallapragada on 2/26/20.
 *
 * Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

 (i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).

 Find the minimum element.
 */
public class FindMinInRoatedArray {

    public static void main(String[] args){

        //int[] nums = {4,5,6,7,0,1,2};
        int[] nums = {2,1};
        System.out.print("Min number at index: " + findMin(nums));
    }

    public static int findMin(int[] nums){
        int low=0;int high = nums.length-1;
        while(low<=high){
            int mid = (low+high)/2;

            if(nums[mid] < nums[mid+1]){
                return mid+1;
            }else if(nums[mid] <= nums[high]){
                //Sorted array
                high = mid -1;
            }else if(nums[low] <= nums[mid]){
                low = mid + 1;
            }

        }
        return 0;
    }
}
