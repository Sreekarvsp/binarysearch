/**
 * Created by sreekarryallapragada on 2/25/20.
 */
public class CountOccurrencesInSortedArray {

    public static void main(String[] args) {
        int[] arr = {2, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5, 5, 6, 6, 7, 7, 7, 7, 10, 10, 10};
        int target = 5;
        int firstOccurence = BinarySearchFirstLastOccurence.firstOccurrence(arr,target);
        int lastOccurrence = BinarySearchFirstLastOccurence.lastOccurrence(arr,target);

        System.out.print("Number of occurrences of "+target+" in given sorted array: "+ (lastOccurrence - firstOccurence + 1));
    }
}
